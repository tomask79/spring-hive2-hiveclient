# Integration of new release of Spring-Hadop 2.3.0.M1 + Hive 1.0.0 + Hadoop 2.6.0 #

New version of Spring-Hadoop integration has been release today with support of 1.x Hive API.

http://spring.io/blog/2015/08/04/spring-for-apache-hadoop-2-3-milestone-1-released?utm_content=buffer53b63&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer

Well, I couldn't resist and had to test it and well done Thomas Rosberg (@trisberg) !

When I first tried Hive support in SHDP integration, I didn't understand why am I forced to use old Hive API...Now we can communicate with Hive 2 server over "old looking" HiveClient, but via JDBC configuration...

## Demo description ##

1) First you need to start your Hive 2 server with your meta store database.  

2) Simply run App class..if you have configured everything correctly **you should see all tables in your default** hive database retrieved via HiveClient communicating with Hive2 server over JDBC.
Something like this:

```
log4j:WARN No appenders could be found for logger (org.springframework.core.env.StandardEnvironment).
log4j:WARN Please initialize the log4j system properly.
log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
SLF4J: Class path contains multiple SLF4J bindings.
SLF4J: Found binding in [jar:file:/Users/tomask79/.m2/repository/org/slf4j/slf4j-log4j12/1.7.5/slf4j-log4j12-1.7.5.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: Found binding in [jar:file:/Users/tomask79/.m2/repository/org/apache/hive/hive-jdbc/1.0.0/hive-jdbc-1.0.0-standalone.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
SLF4J: Actual binding is of type [org.slf4j.impl.Log4jLoggerFactory]
Table name: hivetabletest
Table name: hivetabletest2
Table name: test
Table name: test2
Table name: weather_data
```

### Few notes ###

1) When setting pom dependencies, don't forget to set versions of hadoop and hive according to your environment. For example do not set hive-version 1.2.1 if you have hive 1.0.0 installed otherwise you will end up with error:

**Caused by: org.apache.thrift.TApplicationException: Required field 'client_protocol' is unset! Struct:TOpenSessionReq(client_protocol:null, configuration:{use:database=default})
**

2) Focus at applicationContext file:


```
#!<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context" 
	xmlns:hdp="http://www.springframework.org/schema/hadoop"
	xmlns:p="http://www.springframework.org/schema/p"
	xsi:schemaLocation="
        http://www.springframework.org/schema/beans 
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/hadoop 
        http://www.springframework.org/schema/hadoop/spring-hadoop.xsd">

	<hdp:configuration id="hadoopConfiguration"
		file-system-uri="hdfs://127.0.0.1:9000">
		mapred.job.tracker=127.0.0.1:9010
	</hdp:configuration> 
	
	<hdp:hive-client-factory id="hiveClientFactory" hive-data-source-ref="hiveDataSource"/>

	<bean id="hiveDriver" class="org.apache.hive.jdbc.HiveDriver"/>
	<bean id="hiveDataSource" class="org.springframework.jdbc.datasource.SimpleDriverDataSource">
		<constructor-arg name="driver" ref="hiveDriver"/>
		<constructor-arg name="url" value="jdbc:hive2://localhost:10000/default"/>
	</bean>
	
	<hdp:hive-template id="hiveTemplate"/>
	
	<!-- Test bean for communicating with external hive2 server over HiveClient. -->
	<bean id="testBean" 
		  class="myhadoop.hiveserver.test.TestBean" 
		  p:hive-template-ref="hiveTemplate"/>
	
</beans>
```

See how easy and straightforward is now configuring communication with external hive2 server...