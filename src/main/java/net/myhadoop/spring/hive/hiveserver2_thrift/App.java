package net.myhadoop.spring.hive.hiveserver2_thrift;

import myhadoop.hiveserver.test.TestBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Demo showing how to communicate with Hive 2 server over HiveClient API in 2.3.0-M1...
 * which follows old thrift API in SHDP 2.2.0, but works over JDBC now.
 * 
 * Thank you Thomas Risberg!
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext ctx = new 
        		ClassPathXmlApplicationContext("applicationContext.xml");
        
        TestBean testBean = (TestBean) ctx.getBean("testBean");
        
        for (String db: testBean.getTables()) {
        	System.out.println("Table name: "+db);
        }
    }
}
