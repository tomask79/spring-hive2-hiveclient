package myhadoop.hiveserver.test;

import java.util.List;

import org.springframework.data.hadoop.hive.HiveClient;
import org.springframework.data.hadoop.hive.HiveClientCallback;
import org.springframework.data.hadoop.hive.HiveTemplate;

/**
 * 
 * @author tomask79
 *
 */
public class TestBean {
	private HiveTemplate hiveTemplate;
	
	public void setHiveTemplate(HiveTemplate template) { this.hiveTemplate = template; }
	
	  public List<String> getTables() {
	      return hiveTemplate.execute(new HiveClientCallback<List<String>>() {

			public List<String> doInHive(HiveClient arg0) throws Exception {
				return arg0.execute("show tables");
			}
	      });
	  }
}
